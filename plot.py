import sys

fin = sys.argv[1]

from matplotlib.pylab import *
import numpy
from math import pow, sin, cos, pi

data = numpy.loadtxt(fin)

nr_plots = 6

current_plot = 1

# subplot(nr_plots, 1, current_plot)
# plot(data[:-1, 0], data[:-1, 1])
# ylabel('piece_index')
# grid()
# current_plot +=1

# subplot(nr_plots, 1, 2)
# plot(data[:-1, 0], data[:-1, 2])
# ylabel('in_piece_distance')
# grid()

# data = data[400:1000, :]

angle = data[:-1, 3]
angle_speed = data[:-1, 4]
angle_acceleration = data[:-1, 5]
radius = data[:-1, 6]
speed = data[:-1, 7]
acc = data[:-1, 8]
throttle = data[:-1, 9]
# xk = data[:-1, 8]
# vk = data[:-1, 9]
# ak = data[:-1, 10]
effective_radius = data[:-1, 10]
pred_acc = data[:-1, 11]
pred_err = data[:-1, 12]
pred_angle = data[:-1, 13]
pred_angle_err = data[:-1, 14]
correction = data[:-1, 15]
acc_err_avg = data[:-1, 16]

# for i in xrange(len(angle)):
#     print angle[i], angle_speed[i], angle_acceleration[i]

xmin, xmax = 540, 600

# ka, kas, ks, c = [-0.00736473, -0.10087426,  2.62664786, -0.83912688]
# ka, kas, ks, c = [-0.0077866 , -0.0995619 ,  2.62705159, -0.85810216]

# # ka, kas, ks, c = [-0.006,      -0.08,        2.8,        -0.92965713]
# ka, kas, ks, c = [-0.00739905, -0.10060787,  2.63612879, -0.84267583]

ka, kas, ks, c = [-0.00812456, -0.09999984,  2.64166573, -0.82828684]
# ka, kas, ks, c = [-0.00344972 -0.01637351  0.01619884  0.0219667]
Kt = 0.18962419
Ks = -0.01596899
kacc_a, kacc_s = [-0.00630141,  0.07435372]


ka, kas, ks, c = [-1.24999166e-03, -1.00000054e-01, 2.64163162e+00,
                  -8.36224751e-01]


# subplot(nr_plots, 1, current_plot)
# plot(data[:-1, 0], angle_speed)
# # plot(data[:-1, 0], vk)
# ylabel('angle speed')
# grid()
# # xlim(540, 600)
# current_plot += 1

# subplot(nr_plots, 1, current_plot)
# plot(data[:-1, 0], angle_speed * angle_acceleration)
# # plot(data[:-1, 0], vk)
# ylabel('angle product')
# grid()
# # xlim(540, 600)
# current_plot += 1

sc = []
# estimated_acc = ka * angle + kas * angle_speed
estimated_acc = angle * 0
for i in xrange(1, len(angle_acceleration) - 1):
    estimated_acc[i] = (ka * angle[i - 1] * speed[i - 1] + kas * angle_speed[i - 1])
    acc_comp = (kacc_s * angle_speed[i - 1] + kacc_a * angle[i - 1]) * acc[i - 1]
    acc_comp = 0
    estimated_acc[i] += acc_comp
    if radius[i]:
        c0 = -1.00141720e+00
        c1 = 2.60828293e-02
        c = c0 + c1 * speed[i - 1]
        speed_component = (ks * speed[i - 1] * speed[i - 1]) / abs(radius[i - 1]) + c
        speed_component = max(0, speed_component)
        sc.append(abs(speed_component) * numpy.sign(radius[i - 1]))
        estimated_acc[i] += abs(speed_component) * numpy.sign(radius[i - 1])
    else:
        sc.append(0)
acc_err = angle_acceleration - estimated_acc
# acc_err /= acc
# acc_err = acc_err / angle_speed

d = []
for i in xrange(len(angle_acceleration)):
    d.append((angle[i], acc_err[i]))

d.sort()

# a = [x[0] for x in d[1:]]
# e = [x[1] for x in d[1:]]

# plot(a, e)
# grid()
# show()
# sys.exit(0)

subplot(nr_plots, 1, current_plot)
plot(data[:-1, 0], angle)
# plot(data[:-1, 0], pred_angle)
# plot(data[:-1, 0], pred_angle_err)
# plot(data[:-1, 0], xk)
ylabel('angle')
grid()
# xlim(xmin, xmax)
current_plot += 1

subplot(nr_plots, 1, current_plot)
plot(data[:-1, 0], data[:-1, 5])
plot(data[:-1, 0], pred_acc)
plot(data[:-1, 0], pred_err)
# plot(data[:-1, 0], pred_err)
# plot(data[:-1, 0], correction)
# plot(data[:-1, 0], estimated_acc)
# plot(data[:-1, 0], acc_err)
ylabel('angle acceleration')
grid()
# xlim(xmin, xmax)
current_plot += 1

subplot(nr_plots, 1, current_plot)
plot(data[:-1, 0], data[:-1, 6])
plot(data[:-1, 0], effective_radius)
ylabel('piece radius')
grid()
# xlim(xmin, xmax)
current_plot += 1

subplot(nr_plots, 1, current_plot)
plot(data[:-1, 0], data[:-1, 7])
ylabel('speed')
grid()
current_plot += 1


est_acc = throttle * Kt + speed * Ks
subplot(nr_plots, 1, current_plot)
plot(data[:-1, 0], acc)
# plot(data[:-1, 0], est_acc)
ylabel('acceleration')
grid()
current_plot += 1

subplot(nr_plots, 1, current_plot)
plot(data[:-1, 0], throttle)
ylabel('throttle')
grid()
current_plot += 1

show()

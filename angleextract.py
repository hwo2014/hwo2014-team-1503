import sys

fin = sys.argv[1]

from matplotlib.pylab import *
import numpy

data = numpy.loadtxt(fin)

nr_plots = 5

current_plot = 1

# subplot(nr_plots, 1, current_plot)
# plot(data[:-1, 0], data[:-1, 1])
# ylabel('piece_index')
# grid()
# current_plot +=1

# subplot(nr_plots, 1, 2)
# plot(data[:-1, 0], data[:-1, 2])
# ylabel('in_piece_distance')
# grid()

angle = data[:-1, 3]
angle_speed = data[:-1, 4]
angle_acceleration = data[:-1, 5]
radius = data[:-1, 6]
speed = data[:-1, 7]
# xk = data[:-1, 8]
# vk = data[:-1, 9]
# ak = data[:-1, 10]
effective_radius = data[:-1, 11]

ka = -0.00861092
kas = -0.10247571
estimated_acc = ka * angle + kas * angle_speed
acc_err = angle_acceleration - estimated_acc

selected = []

for i in xrange(1000, len(effective_radius)):
    if radius[i]:
        selected.append((effective_radius[i], acc_err[i]))

selected.sort()

r = [x[0] for x in selected]
e = [x[1] for x in selected]

plot(r, e, '-x')
grid()
show()

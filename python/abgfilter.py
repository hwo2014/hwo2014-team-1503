from __future__ import division

# Inputs:
#   xm - measured parameter
#   dt - time delta (usually 1)
#   xk - last parameter
#   vk - last velocity
#   ak - last acceleration
#   alpha, beta, gamma - filter parameters
#
# Outputs:
#    xkp - next system state (ie: position)
#    vkp - next derivative of system state (ie: velocity)
#    akp - next second derivative of system state (ie: acceleration)
#    rk  - residual error
#
# Result: (xkp, vkp, akp, rk)
def abgfilter(xm, dt, xk, vk, ak, alpha, beta, gamma):
    # update estimated system state x
    xkp = xk + dt * vk + 0.5 * dt * dt * ak

    # update estimated velocity
    vkp = vk + dt * ak

    # residual error
    rk = xm - xkp

    # update x
    xkp = xkp + alpha * rk

    # given residual
    vkp = vkp + beta / dt * rk

    # error
    akp = ak + gamma / (2 * dt * dt) * rk

    return (xkp, vkp, akp, rk)

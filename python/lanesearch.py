import time
import json
from copy import deepcopy
from multiprocessing import Process, Pipe

from prediction import predict, model_tick_simple
from common import Position


class LaneSearch(Process):
    def __init__(self, world, nr_switches, pipe):
        self.world = world
        self.nr_switches = nr_switches
        self.pipe = pipe
        Process.__init__(self)
        self.daemon = True

    def run(self):
        while True:
            pos = self.pipe.recv()
            path = search(self.world, pos, self.nr_switches)
            print 'SENDING RESULT'
            self.pipe.send(path)


def determine_throttle(world, pos):
    critical_angle = 55
    look_ahead = 100
    throttle_vec = [0] * look_ahead
    throttle_vec[0] = 1
    positions = predict(world, pos, throttle_vec)
    angles = [p.angle for p in positions]
    max_angle = max(abs(max(angles)), abs(min(angles)))
    if max_angle < critical_angle:
        return 1.0
    else:
        return 0.0


def add_to_frontier(frontier, pos):
    np = pos
    if pos.lane in frontier:
        p = frontier[pos.lane]
        is_better_position = False
        if np.lap > p.lap:
            is_better_position = True
        elif np.lap == p.lap:
            if np.piece_index > p.piece_index:
                is_better_position = True
            elif np.piece_index == p.piece_index:
                if np.in_piece_distance > p.in_piece_distance:
                    is_better_position = True
        if is_better_position:
            frontier[pos.lane] = pos
    else:
        frontier[pos.lane] = pos


def search(world, pos, look_ahead_switches):
    frontier = {}
    frontier[pos.lane] = pos
    look_ahead = 100
    pos['switches'] = []
    nr_switches_met = 0

    while True:
        new_frontier = {}
        # print 'Tick:', t
        for l in xrange(4):
            if l in frontier:
                p = frontier[l]
                # print 'Lane %d pos: (%d, %5.2f) speed: %5.2f angle: %5.2f %s' % (
                #     l, p.piece_index, p.in_piece_distance, p.speed, p.angle, p.switches)
        # print
        for l, pos in frontier.iteritems():
            throttle = determine_throttle(world, pos)
            new_pos = model_tick_simple(world, pos, throttle, 1)
            add_to_frontier(new_frontier, new_pos)
            if new_pos.piece_index != pos.piece_index:
                ## possible lane switch
                # print 'switch'
                if world.track['pieces'][new_pos.piece_index].get('switch'):
                    nr_switches_met += 1
                    if nr_switches_met == look_ahead_switches:
                        return new_position['switches']
                        ## end of search
                        pass
                    #     print '*** END OF SEARCH ***'
                    # print '*** SWITCH ***'
                    lane = new_pos.lane
                    new_positions = []
                    if lane + 1 < len(world.track['lanes']):
                        new_lane = lane + 1
                        new_position = deepcopy(new_pos)
                        new_position['lane'] = new_lane
                        new_position['switches'].append(
                            (new_pos.piece_index, new_lane))
                        new_positions.append(new_position)
                    if lane > 0:
                        new_lane = lane - 1
                        new_position = deepcopy(new_pos)
                        new_position['lane'] = new_lane
                        new_position['switches'].append(
                            (new_pos.piece_index, new_lane))
                        new_positions.append(new_position)
                    # print 'new_positions:', new_positions
                    for np in new_positions:
                        add_to_frontier(new_frontier, np)
                    # print 'NEW FRONTIER:', frontier
        frontier = new_frontier




                    ## we can switch




def main():
    from main import World

    world = World()
    track = json.loads(open('track.txt').read())
    world.set_track_data(track)
    world.set_own_car_color('red')

    pos = Position(
        piece_index=0,
        in_piece_distance=0,
        lane=1,
        angle=0,
        lap=0,
        speed=0,
        acc=0,
        throttle=0,
        angle_speed=0,
        angle_acceleration=0,
        radius=0,
        lap_distance=0)

    nr_switches = len([p for p in world.track['pieces'] if 'switch' in p])
    path = search(world, pos, nr_switches)
    print path

    print 'DOING PROCESS SEARCH'
    pipe_a, pipe_b = Pipe()
    process = LaneSearch(world, nr_switches, pipe_b)
    process.start()
    pipe_a.send(pos)
    while not pipe_a.poll():
        print 'WAITING'
        time.sleep(1)

    result = pipe_a.recv()
    print result



if __name__ == '__main__':
    main()

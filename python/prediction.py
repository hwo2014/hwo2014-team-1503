from __future__ import division

import math
from numpy import abs, sign

from common import Position
from physics import static_angle_acc, calculate_radius


def model_tick_simple(world, pos, throttle, time_step, breakable=True):
    c = world.constants
    # kaa = c.kaa
    # kaas = c.kaas
    # kas = c.kas
    # ac = c.ac
    kst = c.kst
    kss = c.kss

    lane = pos.lane
    piece_index = pos.piece_index
    in_piece_distance = pos.in_piece_distance
    speed = pos.speed
    angle = pos.angle
    angle_speed = pos.angle_speed

    ## FIXME: throttle or 0 is a terrible hack
    acc = kst * (throttle or 0) + kss * speed
    n_speed = speed + acc * time_step
    n_in_piece_distance = in_piece_distance + n_speed * time_step
    piece = world.track['pieces'][piece_index]
    piece_radius = piece.get('radius')

    radius = calculate_radius(world.track, piece_index, lane)
    angle_acc = static_angle_acc(c, throttle, speed, angle, angle_speed,
                                 radius)

    n_angle_speed = angle_speed + angle_acc * time_step
    n_angle = angle + n_angle_speed * time_step

    piece_length = piece['length']
    if radius:
        piece_length = (2 * math.pi * abs(radius) *
                        abs(piece['angle']) / 360)

    n_piece_index = piece_index
    if n_in_piece_distance > piece_length:

        n_in_piece_distance -= piece_length
        n_piece_index += 1
        if n_piece_index >= len(world.track['pieces']):
            n_piece_index = 0

        if breakable:
            ## Calculate part time
            distance_left = piece_length - in_piece_distance
            time_left = distance_left / n_speed
            if time_left < 1:
                p1 = model_tick_simple(world, pos, throttle, time_left, False)
                p1['piece_index'] = n_piece_index
                p1['in_piece_distance'] = 0
                p2 = model_tick_simple(world, p1, throttle, 1 - time_left, False)
                return p2


    new_pos = Position(pos)
    new_pos['piece_index'] = n_piece_index
    new_pos['in_piece_distance'] = n_in_piece_distance
    new_pos['speed'] = n_speed
    new_pos['angle'] = n_angle
    new_pos['angle_speed'] = n_angle_speed
    new_pos['angle_acceleration'] = angle_acc
    new_pos['piece_radius'] = piece_radius
    return new_pos


def predict(world, pos, throttle_vector):
    positions = []
    current_pos = Position(pos)
    for t in throttle_vector:
        new_pos = model_tick_simple(world, current_pos, t, 1.0)
        positions.append(Position(new_pos))
        current_pos = new_pos
    # return p_angle
    return positions


# def model_tick(world, current_pos, t):
#     subticks = 2
#     time_step = 1 / subticks
#     pos = current_pos
#     for i in xrange(subticks):
#         pos = model_tick_simple(world, pos, t, time_step)
#     return pos

def predict_accurate(world, pos, throttle_vector, switch):
    positions = []
    current_pos = Position(pos)
    before_switch_index = current_pos.piece_index
    switched = False
    for t in throttle_vector:
        new_pos = model_tick_simple(world, current_pos, t, 1)
        if switch and not switched and new_pos.piece_index == before_switch_index + 1:
            switched = True
            new_pos.lane += ( -1 if switch == 'Left' else 1 )
        positions.append(Position(new_pos))
        current_pos = new_pos
    # return p_angle
    return positions

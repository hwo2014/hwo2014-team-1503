from __future__ import division

import pickle
import json
import socket
import sys
import math
import random
import time
import pickle
from multiprocessing import Pipe

from pprint import pprint
from collections import defaultdict, namedtuple

from abgfilter import abgfilter
from common import AttrDict, Position
from prediction import predict, predict_accurate
from staticlanes import find_best_lane
from lanesearch import LaneSearch
from identification import Identification
from physics import calculate_radius, static_angle_acc

import scipy

# Install IPtyhon in virtualenv for fancy exception tracebacks
try:
    from IPython.core import ultratb
    sys.excepthook = ultratb.FormattedTB(
        mode='Verbose', color_scheme='Linux', call_pdb=False)
except ImportError:
    pass


def sign(x):
    if x > 0:
        return 1.0
    if x < 0:
        return -1.0
    return 0.0

def out(format_string, args=None):
    if args is None:
        print format_string
    else:
        print format_string % args

def err(format_string, args=None):
    if args is None:
        print >> sys.stderr, format_string
    else:
        print >> sys.stderr, format_string % args

def progress(s):
    sys.stdout.write("\r%s\r" % s)
    sys.stdout.flush()


class World(object):
    def __init__(self):
        self.enable_world_dump = True

        self.track = None
        self.own_car_color = None
        self.car_positions = defaultdict(list)
        self.last_tick = None
        self.history = defaultdict(list)
        self.turbo = None
        self.crash = False

        self.constants = AttrDict(
            kaa=-1.24959412e-03,
            kaas=-0.10060787,
            kas=2.63612879,
            ac0=-1.00142299e+00,
            ac1=2.60861042e-02,
            kst=0.19199614,
            kss=-0.01918763)

        self.identification_done = False
        self.identification_angle = 50
        self.identification_time = 500
        self.got_identification_data = False

        self.pipe_a, self.pipe_b = Pipe()

        self.acceleration_error = 0
        self.acceleration_alpha = 0.2


    def handle_crash(self, data):
        if data['color'] == self.own_car_color:
            self.crash = True

    def handle_spawn(self, data):
        if data['color'] == self.own_car_color:
            self.crash = False

    def set_track_data(self, track):
        self.track = track

        ## Put length in every piece
        track_length = 0
        for i, piece in enumerate(track['pieces']):
            if 'length' not in piece:
                piece['length'] = (2 * math.pi * piece['radius'] *
                                   abs(piece['angle']) / 360)
            ## Ignoring that there are multiple lanes
            track_length += piece['length']
            piece['index'] = i

        if self.enable_world_dump:
            open('world.json', 'w').write(json.dumps(track, indent=4))

        self.track_length = track_length

    def set_cars_data(self, cars):
        self.cars = cars

    def set_own_car_color(self, color):
        self.own_car_color = color

    def set_turbo(self, data):
        self.turbo = data

    def maybe_do_identification(self):
        if not self.identification_done and self.car_positions[self.own_car_color]:
            pos = self.car_positions[self.own_car_color][-1]
            if (self.last_tick > self.identification_time):
                positions = self.car_positions[self.own_car_color][100:]
                initial_values = AttrDict(self.constants)
                ident = Identification(self.pipe_b)
                ident.start()
                self.pipe_a.send((None, positions, initial_values))
                self.identification_done = True
                print '**** IDENTIFICATION STARTED ****'

    def update_car_positions(self, data, game_tick):

        if self.crash:
            print 'CRASH'

        self.maybe_do_identification()
        if self.pipe_a.poll():
            id_data = self.pipe_a.recv()
            tag, constants = id_data
            if tag is None:
                print '**** GOT IDENTIFICATION DATA ****'
                print 'before:', self.constants
                print constants
                self.constants = constants
                print 'after:', self.constants
                self.got_identification_data = True

        if self.last_tick is not None:
            while self.last_tick + 1 < game_tick:
                for car in data:
                    car_color = car['id']['color']
                    self.car_positions[car_color].append(None)
        self.last_tick = game_tick
        pieces = self.track['pieces']
        for car in data:
            car_color = car['id']['color']
            angle = car['angle']
            if self.car_positions[car_color]:
                last = self.car_positions[car_color][-1]
            else:
                last = None
            piece_index = car['piecePosition']['pieceIndex']
            in_piece_distance = car['piecePosition']['inPieceDistance']
            lap = car['piecePosition']['lap']
            ## A bit simplystic here
            lane = car['piecePosition']['lane']['endLaneIndex']
            angle_speed = 0
            angle_acceleration = 0
            if last is not None:
                angle_speed = angle - last.angle
                angle_acceleration = angle_speed - last.angle_speed
            speed = 0

            piece = pieces[piece_index]
            current_radius = 0
            if 'radius' in piece:
                radius = piece['radius']
                radius_delta = (self.track['lanes'][lane]
                                ['distanceFromCenter'] *
                                sign(piece['angle']))
                current_radius = (radius - radius_delta) * sign(piece['angle'])
                # last_piece_length = (2 * math.pi * lane_radius *
                #                      abs(last_piece['angle']) / 360)

            xk = 0
            vk = 0
            ak = 0
            acc = 0
            predicted_acc = 0
            predicted_acc_err = 0
            if last is not None:
                if last.piece_index == piece_index:
                    speed = in_piece_distance - last.in_piece_distance
                else:
                    last_piece = pieces[last.piece_index]
                    if 'radius' in last_piece:
                        last_piece_length = (2 * math.pi * abs(last.radius) *
                                             abs(last_piece['angle']) / 360)
                    else:
                        last_piece_length = last_piece['length']
                    speed = (last_piece_length - last.in_piece_distance +
                             in_piece_distance)
                acc = speed - last.speed

                angle_acc = angle_speed - last.angle_speed
                r = calculate_radius(self.track, last.piece_index, last.lane)

                if car_color == self.own_car_color:
                    predicted_acc = static_angle_acc(
                        self.constants, last.throttle, last.speed, last.angle,
                        last.angle_speed, r)
                    predicted_acc_err = angle_acc - predicted_acc

                    predicted_acc = predicted_acc
                    predicted_acc_err = angle_acc - predicted_acc
                    if abs(predicted_acc_err) <= 0.5:
                        self.acceleration_error = (
                            self.acceleration_error * (1 - self.acceleration_alpha) +
                            abs(predicted_acc_err) * self.acceleration_alpha)

                    # print 'new_predicted_ac_err:', predicted_acc_err

                    if last.radius != current_radius:
                        predicted_acc_err = 0


            lap_distance = in_piece_distance
            for i in xrange(piece_index):
                lap_distance += self.track['pieces'][i]['length']

            effective_radius = current_radius

            predicted_angle = angle

            # if car_color == self.own_car_color:
            #     if self.got_identification_data:
            #         prediction_range = 30
            #         positions = self.car_positions[car_color][-prediction_range:]
            #         throttle_vector = []
            #         for p in positions:
            #             throttle_vector.append(p.throttle)
            #         # print 'OMGWTFBBQ', len(throttle_vector)
            #         predicted_positions = predict(self, positions[0],
            #                                       throttle_vector)
            #         predicted_angle = predicted_positions[-1].angle

            predicted_angle_err = angle - predicted_angle
            # print 'pae:', predicted_angle_err

            self.car_positions[car_color].append(
                Position(
                    piece_index=piece_index,
                    in_piece_distance=in_piece_distance,
                    lane=lane,
                    angle=angle,
                    lap=lap,
                    speed=speed,
                    acc=acc,
                    throttle=None,
                    angle_speed=angle_speed,
                    angle_acceleration=angle_acceleration,
                    radius=current_radius,
                    lap_distance=lap_distance,
                    effective_radius=effective_radius,
                    predicted_acc=predicted_acc,
                    predicted_acc_err=predicted_acc_err,
                    predicted_angle=predicted_angle,
                    predicted_angle_err=predicted_angle_err,
                    correction=0,
                    acceleration_error=self.acceleration_error))

    def update_throttle(self, throttle):
        pos = self.car_positions[self.own_car_color][-1]
        pos['throttle'] = throttle


class Tyrone(object):
    def __init__(self):
        self.name = 'Tyrone'
        self.mode = None
        # self.mode = 'random'

        # look 100 ticks ahead (not used at the moment)
        self.lookahead_time = 60
        self.max_angle = 30
        # self.target_lane = 1

        self.throttle_level = 0.65

        self.last_lane_switch = None
        self.turbo_stop = None
        self.turbo_duration = 0
        self.turbo_factor = 1

        self.lane_pipe_a, self.lane_pipe_b = Pipe()
        self.lane_plan = {}
        self.nr_switches = None
        self.lane_process = None

        self.turbo_length_factor = 0.8
        self.turbo_length_a = 0.8
        self.ever_used_turbo = False

        self.last_decrease_counter = 0
        self.extra_safety = 0

    def calculate_control(self, world):
        if (world.last_tick > 1000 and (not world.last_tick % 600) and
            not self.ever_used_turbo):
            self.turbo_length_factor *= self.turbo_length_a

        # Be a little less cautious as soon as we have some data
        if (world.last_tick > 800):
            self.max_angle = 53
            self.max_angle = 50

        # Be a little less cautious as soon as we have some data
        if (world.last_tick > 1000):
            self.max_angle = 55
            self.max_angle = 50


        full_angle_time = 1800

        # Now go full angle
        if (world.last_tick > full_angle_time):
            self.max_angle = 57
            self.max_angle = 50

        # Calculate the number of switches
        if self.nr_switches is None:
            self.nr_switches = len(
                [p for p in world.track['pieces'] if 'switch' in p])

        color = world.own_car_color

        # No data yet? Full throttle!
        if not len(world.car_positions[color]):
            return 1.0, 1.0, None

        pos = world.car_positions[color][-1]

        if (world.last_tick >= 100):
            if abs(pos.angle) >= self.max_angle and not self.last_decrease_counter:
                self.extra_safety += 1
                self.last_decrease_counter = 200

        if self.last_decrease_counter:
            self.last_decrease_counter -= 1

        # Start lane search process
        if self.lane_process is None:
            self.lane_process = LaneSearch(
                world, self.nr_switches, self.lane_pipe_b)
            self.lane_process.start()
            self.lane_pipe_a.send(pos)

        # There may be a new lane plan waiting for us
        if self.lane_pipe_a.poll():
            lane_plan = self.lane_pipe_a.recv()
            self.lane_plan = dict(lane_plan)
            self.lane_pipe_a.send(pos)
            print '**** RECEIVED LANE PLAN ****'

        self.lane_plan = {}

        # Check lane plan if we have to switch
        switch = None
        target_lane = None
        next_piece_index = ((pos.piece_index + 1) %
                            len(world.track['pieces']))
        next_piece = world.track['pieces'][next_piece_index]
        if next_piece_index in self.lane_plan:
            target_lane = self.lane_plan[next_piece_index]

        # target_lane = find_best_lane(world, pos)

        # FIXME: remove this!!!!
        #target_lane = 0

        # angle_safety further reduces max_angle
        # angle_safety = 3
        angle_safety = max(
            1,
            min(16,
                (self.max_angle - abs(pos.angle)) / 5))

        # angle_safety = 5
        # angle_safety += min(pos.acceleration_error * 100, 10)
        print 'angle_safety:', angle_safety

        if (next_piece.get('switch') and
            target_lane is not None and
            self.last_lane_switch != pos.piece_index):
            self.last_lane_switch = pos.piece_index
            if pos.lane < target_lane:
                switch = 'Right'
            if pos.lane > target_lane:
                switch = 'Left'

            if switch is not None:
                switch_lookahead = 1
                throttle_vec = ([self.turbo_factor] * switch_lookahead +
                                [0] * self.lookahead_time)
                positions = predict_accurate(world, pos, throttle_vec, switch)
                max_angle = max( [abs( p.angle ) for p in positions] )
                print 'CHECKING SWITCH'
                lookahead_distance = self.lookahead_time * pos.speed
                switch_safety = self.calculate_switch_safety(
                    world.track, pos, switch)
                # Cancel switch if it's too dangerous
                limit = self.max_angle - switch_safety - angle_safety - self.extra_safety
                if not check_positions(positions, limit):
                    switch = None

        # Determine throttle
        # if world.last_tick > 1500:
        if False:
            # The list of strategies to avoid falling off track
            throttle_vectors = []
            checks = 5
            for i in xrange( checks ):
                # Basic strategy: Full brake
                throttle_vectors.append( [ self.turbo_factor * i / (checks - 1) ] + ( [0] * self.lookahead_time ) )

                # Basic strategy: Set throttle to 0.5
                #throttle_vectors.append( [ self.turbo_factor * i / 10 ] + ( [0.5] * self.lookahead_time ) )

            choices = []
            for vec in throttle_vectors:
                positions = predict_accurate(world, Position(pos), vec, switch)
                angle_ok = [ abs(p.angle) < self.max_angle - angle_safety for p in positions ]
                if False in angle_ok:
                    index = angle_ok.index( False )
                    maxDistance = index - abs(positions[index + 1].angle)
                else:
                    maxDistance = self.lookahead_time
                choices.append((
                    # The throttle we would set
                    vec[ 0 ] / self.turbo_factor,
                    # How far this would get us
                    maxDistance
                ))

            # Keep the choices that get us the furthest
            if maxDistance == self.lookahead_time:
                maxDistance = max( c[1] for c in choices )
            else:
                maxDistance = min( c[1] for c in choices )
            # sys.stdout.write( " maxDistance %3s " % maxDistance )
            # sys.stdout.write( " choices %s " % choices )
            choices = [ c for c in choices if c[1] == maxDistance ]

            # Now select the max throttle of the remaining choices
            throttle = max( [ c[0] for c in choices ] )
        else:
            throttle = 0
            throttle_vec = [0] * self.lookahead_time
            extra_lookahead = 0
            for i in xrange(1 + extra_lookahead):
                throttle_vec[i] = self.turbo_factor

            # Can we set throttle to one?
            positions = predict_accurate(world, pos, throttle_vec, switch)
            limit = self.max_angle - angle_safety - self.extra_safety
            if check_positions(positions, limit):
                throttle = 1.0
            # max_angle = max( [abs( p.angle ) for p in positions] )
            # if max_angle < self.max_angle - angle_safety:
            #     throttle = 1.0

            # # If not one, can we use 0.3?
            # if throttle == 0:
            #     throttle_vec[i] = self.turbo_factor * 0.3
            #     positions = predict_accurate(world, pos, throttle_vec, switch)
            #     max_angle = max( [abs( p.angle ) for p in positions] )
            #     if max_angle < self.max_angle - angle_safety:
            #         throttle = 0.3

            # # If not 0.3, can we use 0.1?
            # if throttle == 0:
            #     throttle_vec[i] = self.turbo_factor * 0.1
            #     positions = predict_accurate(world, pos, throttle_vec, switch)
            #     max_angle = max( [abs( p.angle ) for p in positions] )
            #     if max_angle < self.max_angle - angle_safety:
            #         throttle = 0.1
        # sys.stdout.write( " throttle: %3s angle: %2.5f" % ( throttle, pos.angle ) )

        # Turbo calculation
        # world.turbo = None
        turbo_safety = 10
        if world.turbo is not None and self.turbo_stop is None:
            for x in xrange(1):
                factor = 1
                throttle_vec = (
                    [world.turbo['turboFactor']] *
                    (int(world.turbo['turboDurationTicks'] *
                         self.turbo_length_factor)) +
                    [0] * self.lookahead_time)
                positions = predict_accurate(world, pos, throttle_vec, switch)
                max_angle = max( [abs( p.angle ) for p in positions] )
                if max_angle < self.max_angle - turbo_safety:
                    # throttle = 'turbo'
                    self.turbo_stop = x * factor
                    print '**** ACTIVATING TURBO ****'
            # throttle = 'turbo'

        if self.turbo_duration:
            self.turbo_duration -= 1
        else:
            self.turbo_factor = 1

        recorded_throttle = self.turbo_factor * throttle

        if self.turbo_stop == 0:
            self.turbo_stop = None
            throttle = 'turbo'
        elif self.turbo_stop is not None:
            self.turbo_stop -= 1
            throttle = 0
        if throttle == 'turbo':
            self.turbo_duration = world.turbo['turboDurationTicks']
            self.turbo_factor = world.turbo['turboFactor']
            self.ever_used_turbo = True
            self.recorded_throttle = self.turbo_factor

        if self.mode == 'random':
            if not (world.last_tick % 50):
                self.throttle_level = random.choice(
                    [0.37 + i * 0.03 for i in xrange(9)])
            throttle = self.throttle_level

        # throttle = 0.64
        # switch = None
        # recorded_throttle = throttle

        # return value: first element - throttle command to be sent
        # to the server
        # second element - throttle for tracking purposes
        # third element - switch command
        return throttle, recorded_throttle, switch

    def calculate_switch_safety(self, track, pos, switch):
        lookahead_distance = self.lookahead_time * pos.speed / 7

        turn_safety = 0
        switch_safety = 10
        piece_index = pos.piece_index
        dist = pos.in_piece_distance
        while lookahead_distance > 0:
            cp = track['pieces'][piece_index]
            piece_angle = cp.get('angle')
            if piece_angle is not None:
                if piece_angle > 0 and switch == 'Right':
                    switch_safety = turn_safety
                    break
                if piece_angle < 0 and switch == 'Left':
                    switch_safety = turn_safety
                    break
            distance_left = cp['length'] - dist
            lookahead_distance -= distance_left
            piece_index = (piece_index + 1) % len(track['pieces'])
            dist = 0
        return switch_safety


def check_positions(positions, limit):
    found_bad_position = False
    # return max([abs(p.angle) for p in positions]) < limit
    for p in positions:
        if p.angle > limit and p.angle_speed > 0:
            found_bad_position = True
            break
        if p.angle < -limit and p.angle_speed < 0:
            found_bad_position = True
            break
    return not found_bad_position


class NoobBot(object):

    def __init__(self, socket, name, key, track, extra=None):
        self.socket = socket
        self.key = key
        self.track = track
        self.game_tick = 0

        self.world = World()
        if extra is None:
            self.throttle_level = None
        else:
            self.throttle_level = float(extra)

        self.control = Tyrone()
        self.name = self.control.name

    def msg(self, msg_type, data, game_tick=None):
        content = {"msgType": msg_type, "data": data}
        content['gameTick'] = game_tick
        self.send(json.dumps(content))

    def send(self, msg):
        self.socket.sendall(msg + '\n')


    def create(self):
        return self.msg('createRace',
                        {'botId': {'name': self.name,
                                   'key': self.key},
                         'trackNname': 'keimola',
                         'password': 'secretpassword',
                         'carCount': 0})

    def join_race(self, track='keimola'):
        return self.msg('joinRace',
                        {'botId': {'name': self.name,
                                   'key': self.key},
                         'trackName': track,
                         'password': 'secretpassword',
                         'carCount': 1})

    def join(self):
        return self.msg('join', {'name': self.name,
                                 'key': self.key})

    def throttle(self, throttle, doc_throttle):
        # out(' throttle %s' % throttle)
        # self.msg('throttle', throttle)
        self.world.update_throttle(doc_throttle)
        self.msg('throttle', throttle, self.game_tick)

    def turbo(self, doc_throttle):
        # self.msg('throttle', throttle)
        self.world.update_throttle(doc_throttle)
        self.msg('turbo', '', self.game_tick)

    def switch(self, switch):
        out(' switch %s' % switch)
        # self.msg('throttle', throttle)
        self.msg('switchLane', switch)

    def ping(self):
        self.msg('ping', {})

    def run(self):
        if self.track is None:
            self.join()
        else:
            self.join_race(self.track)
        # self.create()
        # self.join_race(self.track)
        self.msg_loop()

    def on_join(self, data):
        out('Joined')
        # self.ping()

    def on_join_race(self, data):
        out('Race joined')
        # self.ping()

    def on_create_race(self, data):
        out('Race created')
        # self.ping()
        time.sleep(3)
        self.join()

    def on_game_init(self, data):
        out('Game init')
        self.world.set_track_data(data['race']['track'])
        self.world.set_cars_data(data['race']['cars'])

    def on_your_car(self, data):
        out('Your car')
        self.world.set_own_car_color(data['color'])

    def on_game_start(self, data, game_tick):
        out('Race started')
        # self.ping()

    def on_car_positions(self, data, game_tick=None):
        game_tick = game_tick or 0
        self.game_tick = game_tick
        self.world.update_car_positions(data, game_tick)
        position = self.world.car_positions[self.world.own_car_color][-1]
        turbo_str = ''
        if self.world.turbo is not None:
            turbo_str = ' turbo'
        sys.stdout.write(
            '\rGame tick: %4s lap: %3s distance: (%s, %5.2f) lane: %s %s ' %
            (game_tick, position.lap, position.piece_index,
             position.in_piece_distance, position.lane, turbo_str))
        sys.stdout.flush()
        throttle, doc_throttle, switch = self.control.calculate_control(
            self.world)
        if switch is not None:
            self.switch(switch)
        else:
            if throttle == 'turbo':
                print '**** TURBO ****'
                self.world.set_turbo(None)
                self.turbo(doc_throttle)
            else:
                self.throttle(throttle, doc_throttle)

        report_telemetry(
            self.world, game_tick,
            self.world.car_positions[self.world.own_car_color])

    def on_turbo_available(self, data, game_tick):
        print '*** GOT TURBO ***'
        self.world.set_turbo(data)

    def on_crash(self, data, game_tick):
        out('\nSomeone crashed')
        self.world.handle_crash(data)
        # self.ping()

    def on_spawn(self, data, game_tick):
        out('\nSomeone spawned')
        self.world.handle_spawn(data)

    def on_game_end(self, data):
        print '\nRace ended'
        # self.ping()

    def on_error(self, data):
        out('Error: %s', data)
        # self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'joinRace': self.on_join_race,
            'createRace': self.on_create_race,
            'turboAvailable': self.on_turbo_available,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        last_empty = False
        # out('x')
        while True:
            # out('xxx')
            if not line:
                last_empty = True
                if not last_empty:
                    out('Empty line')
                break
            else:
                if last_empty:
                    last_empty = False
                    print ''

                msg = json.loads(line)
                msg_type, data = msg['msgType'], msg['data']
                game_tick = msg.get('gameTick')
                if msg_type in msg_map:
                    if game_tick is not None:
                        msg_map[msg_type](data, game_tick)
                    else:
                        msg_map[msg_type](data)
                else:
                    out('Got %s', msg_type)
                    out(msg)
                    # self.ping()
                line = socket_file.readline()

def report_telemetry(world, game_tick, car_positions):
    if not car_positions:
        return
    last = car_positions[-1]
    radius = last.radius
    err('%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' %
        (game_tick, last.piece_index, last.in_piece_distance, last.angle,
         last.angle_speed, last.angle_acceleration, radius,
         last.speed, last.acc, last.throttle or 0, last.effective_radius,
         last.predicted_acc, last.predicted_acc_err, last.predicted_angle,
         last.predicted_angle_err, last.correction, last.acceleration_error))

if __name__ == '__main__':
    print sys.argv
    if len(sys.argv) != 5 and len(sys.argv) != 6:
        print('Usage: ./run host port botname botkey')
    else:
        host, port, name, key = sys.argv[1:5]
        try:
            extra = sys.argv[5]
        except IndexError:
            extra = None
        out('Connecting with parameters:')
        out('host={0}, port={1}, bot name={2}, key={3}'.format(*sys.argv[1:5]))
        out('extra: %s', [extra])
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        out('Connected')
        # track = 'keimola'
        # track = 'france'
        # track = 'germany'
        track = 'imola'
        # track = None
        bot = NoobBot(s, name, key, track, extra)
        out('Running a bot')
        bot.run()

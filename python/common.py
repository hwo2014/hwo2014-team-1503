class AttrDict(dict):
    def __getattr__(self, attr):
        # try:
        #     getattr(self, attr)
        # except AttributeError:
        # try:
        if dict.has_key(self, attr):
            return self.get(attr)
        else:
            return dict.__getattr__(self, attr)
        # except KeyError:
        #     raise AttributeError()


class PieceHistory(AttrDict):
    pass


class Position(AttrDict):
    pass

import json
import time

from prediction import predict
from main import World, Position
from matplotlib.pylab import *



def main():
    world = World()
    track = json.loads(open('track.txt').read())
    world.set_track_data(track)
    world.set_own_car_color('red')

    pos = Position(
        piece_index=13,
        in_piece_distance=50,
        lane=1,
        angle=0,
        lap=0,
        speed=10,
        acc=0,
        throttle=1,
        angle_speed=0,
        angle_acceleration=0,
        radius=0,
        lap_distance=0)
    # world.car_positions['red'].append(
    #     )

    throttle = [0] * 100
    throttle[0] = 1

    start = time.time()
    positions = predict(world, pos, throttle)
    end = time.time()
    print '%5.2f' % ((end - start) * 1000)

    angle = [p.angle for p in positions]
    angle_acc = [p.angle_acceleration for p in positions]
    speed = [p.speed for p in positions]
    radius = [p.radius for p in positions]
    piece_radius = [p.get('piece_radius', 0) for p in positions]

    if True:
        print angle
        subplot(5, 1, 1)
        plot(angle, '-x')
        subplot(5, 1, 2)
        plot(angle_acc, '-o')
        subplot(5, 1, 3)
        plot(speed, '-o')
        subplot(5, 1, 4)
        plot(radius, '-o')
        subplot(5, 1, 4)
        plot(piece_radius, '-o')
        show()



if __name__ == '__main__':
    main()

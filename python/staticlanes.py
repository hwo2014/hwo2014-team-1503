def find_best_lane(world, pos):
    nr_pieces = len(world.track['pieces'])
    piece_index = (pos.piece_index + 1) % nr_pieces
    while True:
        piece = world.track['pieces'][piece_index]
        if 'angle' in piece:
            if piece['angle'] > 0:
                return 0
            else:
                return len(world.track['lanes'])
        piece_index = (piece_index + 1) % nr_pieces

from numpy import abs, sign, array, dot
from numpy.linalg import norm
from collections import defaultdict
import scipy.optimize

from common import AttrDict

from multiprocessing import Process, Pipe


## Computes actual signed radius from track data, piece_index and a lane
def calculate_radius(track, piece_index, lane):
    piece = track['pieces'][piece_index]
    radius = 0
    if 'radius' in piece:
        piece_radius = piece['radius']
        radius_delta = (track['lanes'][lane] ['distanceFromCenter'] *
                        sign(piece['angle']))
        radius = abs(piece_radius - radius_delta) * sign(piece['angle'])
    return radius


def static_angle_acc(constants, throttle, speed, angle, angle_speed, radius):
    c = constants
    kaa = c.kaa
    kaas = c.kaas
    kas = c.kas
    ac0 = c.ac0
    ac1 = c.ac1
    # kst = c.kst
    # kss = c.kss

    angle_acc = kaa * angle * speed + kaas * angle_speed
    if radius:
        angle_acc += abs(max(0,
                             kas * speed * speed / abs(radius) +
                             ac0 + ac1 * speed)) * sign(radius)

    return angle_acc


def get_bin(max_value, nr_bins, value):
    width = 2 * max_value / nr_bins
    b = float((value + max_value + width / 2)) / max_value / 2 * nr_bins
    b_int = int(round(b))
    b_int = max(0, b_int)
    b_int = min(b_int, nr_bins - 1)
    return b_int


def fitting_error(x, data):
    ka, kas, kacc, k = x
    mul = array([ka, kas, kacc, k, -1])
    total = 0
    for d in data:
        e = mul.dot(d)
        total += norm(e)
    return total / len(data)


class CorrectionProcess(Process):
    def __init__(self, pipe):
        self.pipe = pipe
        self.correction = Correction()
        Process.__init__(self)
        self.daemon = True

    def run(self):
        while True:
            data = self.pipe.recv()
            while self.pipe.poll():
                data = self.pipe.recv()
            angle, angle_speed, speed, acc, radius, error = data
            optimization = self.correction.update(
                angle, angle_speed, speed, acc, radius, error)
            self.pipe.send((angle, angle_speed, speed, acc, radius,
                            optimization))

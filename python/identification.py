from multiprocessing import Process, Pipe
from math import pow
from numpy import *
import pickle
import scipy.optimize
from common import AttrDict


class Identification(Process):
    def __init__(self, pipe):
        self.pipe = pipe
        Process.__init__(self)
        self.daemon = True

    def run(self):
        while True:
            data = self.pipe.recv()
            # while self.pipe.poll():
            #     data = self.pipe.recv()
            tag, positions, initial = data
            values = self.do_identification(positions, initial)
            self.pipe.send((tag, values))

    def do_identification(self, positions, initial):
        angle = array([p.angle for p in positions])
        angle_speed = array([p.angle_speed for p in positions])
        angle_acc = array([p.angle_acceleration for p in positions])
        speed = array([p.speed for p in positions])
        acc = array([p.acc for p in positions])
        throttle = array([p.throttle for p in positions])
        radius = array([p.radius for p in positions])

        result = scipy.optimize.fmin(
            angle_acc_err,
            [initial.kaa, initial.kaas, initial.kas, initial.ac0, initial.ac1],
            [angle, angle_speed, angle_acc, speed, radius],
            maxiter=1000)
        kaa, kaas, kas, ac0, ac1 = result

        result = scipy.optimize.fmin(
            acc_err,
            [initial.kst, initial.kss],
            [speed, acc, throttle],
            maxiter=1000)
        kst, kss = result

        values = AttrDict(
            kaa=kaa,
            kaas=kaas,
            kas=kas,
            ac0=ac0,
            ac1=ac1,
            kst=kst,
            kss=kss)
        return values


## this works well for finding constants for angular acceleration
def angle_acc_err(x, angle, angle_speed, angle_acc, speed, radius):
    ka, kas, ks, c0, c1 = x

    total = 0

    for i in xrange(len(angle) - 2):
        angle_component = ka * angle[i] * speed[i] + kas * angle_speed[i]
        speed_component = 0
        try:
            if radius[i] and radius[i] == radius[i - 1] and radius[i] == radius[i + 1] and radius[i] == radius[i + 2] and radius[i] == radius[i - 2]:
                speed_component = (
                    ks * pow(speed[i], 2) /
                    abs(radius[i])
                ) + c0 + c1 * speed[i]
                speed_component = max(0, speed_component)
                aa = angle_component + abs(speed_component) * sign(radius[i])
                total += (angle_acc[i + 1] - aa) ** 2
        except IndexError:
            pass
        if not radius[i]:
            aa = angle_component
            total += (angle_acc[i + 1] - aa) ** 2
    return total


def acc_err(x, speed, acc, throttle):
    Kt, Ks = x

    total = 0

    for i in xrange(len(speed) - 2):
        t = throttle[i] or 0
        s = speed[i] or 0
        estimated_acc = t * Kt + Ks * s
        if abs(acc[i + 1]) < 0.5:
            err = acc[i + 1] - estimated_acc
            total += err * err
    return total


def main():
    from main import World

    world = World()
    data = open('/tmp/positions.pickle', 'rb').read()
    positions = pickle.loads(data)
    pipe_a, pipe_b = Pipe()

    for i in xrange(len(positions)):
        max_index = i
        p = positions[i]
        if abs(p.angle) >= 50:
            break

    selected_positions = positions[1:max_index]
    initial_values = AttrDict(
        kaa=-0.006,
        kaas=-0.08,
        kas=2.63,
        ac=-0.84,
        kst=0.19,
        kss=-0.19)

    ident = Identification(initial_values, selected_positions, pipe_b)
    ident.start()

    result = pipe_a.recv()
    print 'RESULT:', result


if __name__ == '__main__':
    main()

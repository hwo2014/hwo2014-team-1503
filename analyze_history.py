import sys
import json
from collections import defaultdict

from matplotlib.pylab import *


def main():
    piece_nr = int(sys.argv[1])

    history = defaultdict(list)

    piece_indexes = set()
    fin = open('history.txt')
    for line in fin.readlines():
        piece_history = json.loads(line.strip())
        history[piece_history['piece_index']].append(piece_history)
        piece_indexes.add(piece_history['piece_index'])

    for index in xrange(min(piece_indexes), max(piece_indexes) + 1):
        history[index].sort(key=lambda x: x['entry_speed'])
        for p in history[index]:
            pass
            # print '%s %5.2f %5.2f %s' % (index, p['entry_speed'], p['angle_delta'],
            #                        p['piece_radius'])

    piece_history = history[piece_nr]#
    measurements = []
    for p in piece_history:
        measurements.append((p['entry_speed'], p['angle_delta']))
    measurements.sort()

    speed = []
    angle_delta = []
    for s, a in measurements:
        speed.append(s)
        angle_delta.append(a)

    plot(speed, angle_delta, '-x')
    xlabel('speed')
    ylabel('angle_delta')
    title('piece: %s radius %s' % (piece_nr,
                                   history[piece_nr][0]['piece_radius']))
    grid()
    show()


if __name__ == '__main__':
    main()

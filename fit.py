import time
import random
import sys
import scipy.optimize
import err

fin = sys.argv[1]

from matplotlib.pylab import *
import numpy

data = numpy.loadtxt(fin)

nr_plots = 5

current_plot = 1

# data = data[1000:4000, :]

angle = data[:-1, 3]
angle_speed = data[:-1, 4]
angle_acc = data[:-1, 5]
radius = data[:-1, 6]
speed = data[:-1, 7]
acc = data[:-1, 8]
throttle = data[:-1, 9]
effective_radius = data[:-1, 10]


start = time.time()

result = scipy.optimize.fmin(
    err.err6,
    # [-0.006 / 6.5, -0.08, 2.8, -1.00140244e+00, 2.60821532e-02, 0],
  #  [ -1.23833175e-03,  -9.97223894e-02,   2.91768045e+00,  -7.37878891e-01,
  # -3.66604961e-02,   -2.79968965e-03] ,
   # [ -1.24983937e-03,  -9.99125353e-02,   2.64390384e+00,  -1.00155517e+00,
   # 2.60807648e-02,  -3.15942067e-03] ,
    [ -1.24959412e-03,  -9.99544736e-02,   2.64358410e+00,  -1.00142299e+00,
   2.60861042e-02],
    [angle, angle_speed, angle_acc, speed, acc, radius, effective_radius],
    maxiter=1000)

end = time.time()

print result
print 'Total time: %5.2f' % (end - start)


print 'Fitting acceleration'
start = time.time()

result = scipy.optimize.fmin(
    err.acc_err,
    [10, -1],
    [speed, acc, throttle],
    maxiter=1000)

end = time.time()

print result
print 'Total time: %5.2f' % (end - start)

from math import pow
from numpy import *

# scipy.optimize.fmin(err.err5, [0, 0, 0], [angle, angle_speed, angle_acc, speed, radius])

## this works well for finding constants for angular acceleration
def err6(x, angle, angle_speed, angle_acc, speed, acc, radius, effective_radius):
    ka, kas, ks, c0, cs = x
    kd = 0
    kla = 0
    # cs = 0

    # ka, kas = -0.00639178, -0.08001387
    # ka = -0.00802026
    # kas = -0.10310667

    total = 0

    for i in xrange(len(angle) - 2):
        angle_component = ka * angle[i] * speed[i] + kas * angle_speed[i]
        acc_component = kla * angle[i] * acc[i]
        acc_component = 0
        angle_component += acc_component
        speed_component = 0
        try:
            if radius[i] and radius[i] == radius[i - 1] and radius[i] == radius[i + 1] and radius[i] == radius[i + 2] and radius[i] == radius[i - 2]:
            # if radius[i]:
                rd = effective_radius[i] - radius[i]
                c = c0 + cs * speed[i]
                speed_component = (
                    ks * pow(speed[i], 2) /
                    # ks * speed[i] / abs(radius[i]) +
                    # ks2 * speed[i] / abs(radius[i]) / abs(radius[i]) -
                    (abs(radius[i]) + kd * rd)
                ) + c
                speed_component = max(0, speed_component)
                # speed_component /= abs(effective_radius[i])
                aa = angle_component + abs(speed_component) * sign(radius[i])
                total += (angle_acc[i + 1] - aa) ** 2
        except IndexError:
            pass
        if not radius[i]:
            aa = angle_component
            total += (angle_acc[i + 1] - aa) ** 2
    return total


def acc_err(x, speed, acc, throttle):
    Kt, Ks = x

    total = 0

    for i in xrange(len(speed) - 2):
        estimated_acc = throttle[i] * Kt + Ks * speed[i]
        if abs(acc[i + 1]) < 0.5:
            err = acc[i + 1] - estimated_acc
            total += err * err
    return total

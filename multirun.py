import os

def main():
    for i in xrange(60, 63):
        throttle = float(i) / 100
        cmd = ('./run testserver.helloworldopen.com 8091 '
               'foo %s 2> telemetry.txt' % throttle)
        print 'Executing:', cmd
        os.system(cmd)
        os.system('cat python/history.txt >> history.txt')
        print
        print


if __name__ == '__main__':
    main()

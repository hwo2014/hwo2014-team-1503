from matplotlib.pylab import *
import numpy
import sys

def main():
    fin = sys.argv[1]
    data = numpy.loadtxt(fin)

    angle_data = list(data[:-1, 3])
    angle_acceleration_data = list(data[:-1, 5])
    radius_data = list(data[:-1, 6])
    speed_data = list(data[:-1, 7])

    max_speed = max(speed_data)

    different_radiuses = list(set(radius_data))
    different_radiuses.sort()
    print 'different radiuses:', different_radiuses

    split_data = {}


    last_radius = None
    for selected_radius in different_radiuses:
        print selected_radius
        current = []

        for radius, angle, angle_acceleration, speed in zip(
                radius_data, angle_data, angle_acceleration_data, speed_data):
            if (radius == selected_radius and radius == last_radius and
                speed >= 0.99 * max_speed):
                current.append((angle, angle_acceleration))
            last_radius = radius

        current.sort()
        split_data[selected_radius] = current


    for i, selected_radius in enumerate(different_radiuses):
        d = split_data[selected_radius]
        a = [p[0] for p in d]
        aa = [p[1] for p in d]

        subplot(len(different_radiuses), 1, i + 1)
        plot(a, aa, '-x')
        title('radius: %s' % selected_radius)
        ylabel('angle_acceleration')
        if i == len(different_radiuses) - 1:
            xlabel('angle')
        grid()


    show()

if __name__ == '__main__':
    main()
